package pl.piotr.cardsapp.game

import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import pl.piotr.cardsapp.TestSchedulerRule
import pl.piotr.cardsapp.game.services.DefaultGameService
import pl.piotr.cardsapp.shared.services.api.models.game.GameSessionApiModel
import pl.piotr.cardsapp.shared.services.api.DeckOfCardsApiService
import pl.piotr.cardsapp.shared.services.api.models.Failed
import pl.piotr.cardsapp.shared.services.api.models.Result
import pl.piotr.cardsapp.shared.services.api.models.Success
import java.util.concurrent.TimeUnit

class GameServiceTests {

    @get:Rule
    var mockitoRule = MockitoJUnit.rule()

    @Rule
    @JvmField
    var testSchedulerRule = TestSchedulerRule()

    @Mock
    lateinit var cardsApiService: DeckOfCardsApiService

    @InjectMocks
    lateinit var mainService: DefaultGameService

    @Test
    fun `Game service tests - example test`() {

        val model = GameSessionApiModel()

        `when`(cardsApiService.shuffleNewDecks(1))
            .thenReturn(Single.just(Result.Success(model)))

        val testObserver = mainService.shuffleNewDecks(1).test()
        testSchedulerRule.testScheduler.advanceTimeBy(6, TimeUnit.SECONDS)

        testObserver
            .assertNoErrors()
            .assertValue {
                when (it) {
                    is Success -> it.data == model
                    is Failed -> false
                }
            }
    }
}
