package pl.piotr.cardsapp.game

import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Assert.assertFalse
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import pl.piotr.cardsapp.TestSchedulerRule
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.card4C
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.card4D
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.card4H
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.cardAS
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.cardJS
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.cardKD
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.cardQC
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.cardQD
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.correctDealModel
import pl.piotr.cardsapp.game.GameTestsConstants.Companion.correctSessionModel
import pl.piotr.cardsapp.game.services.DefaultGameService
import pl.piotr.cardsapp.game.viewModels.GameViewModel
import pl.piotr.cardsapp.shared.services.api.models.game.GameSessionApiModel
import pl.piotr.cardsapp.shared.services.api.models.Failed
import pl.piotr.cardsapp.shared.services.api.models.Result
import pl.piotr.cardsapp.shared.services.api.models.Success
import pl.piotr.cardsapp.shared.services.api.models.game.GameDealApiModel
import java.util.concurrent.TimeUnit

class GameViewModelTests {

    @get:Rule
    var mockitoRule = MockitoJUnit.rule()

    @Rule
    @JvmField
    var testSchedulerRule = TestSchedulerRule()

    @Mock
    lateinit var mainService: DefaultGameService

    @InjectMocks
    lateinit var viewModel: GameViewModel

    //New Game
    @Test
    fun `Game viewModel - New game with success result`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4C, cardJS, cardKD, cardAS)
        })

        playGame()

        assertEquals(viewModel.sessionId.get(), correctSessionModel.deckId)
        assertEquals(viewModel.allCardsInGame.get(), correctSessionModel.remaining)
        assertEquals(viewModel.remainingCards.get(), correctDealModel.remaining)
        assertEquals(viewModel.cards.size, correctDealModel.cards.size)
    }

    @Test
    fun `Game viewModel - New game with error result 1`() {
        mockFailedShuffleNewDecks(1)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4C, cardJS, cardKD, cardAS)
        })

        playGame()

        assertEquals(viewModel.sessionId.get(), null)
        assertEquals(viewModel.allCardsInGame.get(), 0)
        assertEquals(viewModel.remainingCards.get(), 0)
        assertEquals(viewModel.cards.size, 0)
    }

    @Test
    fun `Game viewModel - New game with error result 2`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockFailedDrawCards(correctSessionModel.deckId!!, 5)

        playGame()

        assertEquals(viewModel.sessionId.get(), correctSessionModel.deckId)
        assertEquals(viewModel.allCardsInGame.get(), correctSessionModel.remaining)
        assertEquals(viewModel.remainingCards.get(), correctSessionModel.remaining)
        assertEquals(viewModel.cards.size, 0)
    }

    //Continue Game
    @Test
    fun `Game viewModel - continue game with success result`() {
        viewModel.sessionId.set(correctSessionModel.deckId!!)
        viewModel.remainingCards.set(correctSessionModel.remaining)
        viewModel.allCardsInGame.set(correctSessionModel.remaining)

        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4C, cardJS, cardKD, cardAS)
        })

        playGame()

        assertEquals(viewModel.sessionId.get(), correctDealModel.deckId)
        assertEquals(viewModel.allCardsInGame.get(), correctSessionModel.remaining)
        assertEquals(viewModel.remainingCards.get(), correctDealModel.remaining)
        assertEquals(viewModel.cards.size, correctDealModel.cards.size)
    }

    @Test
    fun `Game viewModel - continue game with error result`() {
        viewModel.sessionId.set(correctSessionModel.deckId!!)
        viewModel.remainingCards.set(correctSessionModel.remaining)
        viewModel.allCardsInGame.set(correctSessionModel.remaining)

        mockFailedDrawCards(correctSessionModel.deckId!!, 5)

        playGame()

        assertEquals(viewModel.sessionId.get(), correctSessionModel.deckId)
        assertEquals(viewModel.allCardsInGame.get(), correctSessionModel.remaining)
        assertEquals(viewModel.remainingCards.get(), correctSessionModel.remaining)
        assertEquals(viewModel.cards.size, 0)
    }


    //Reset Game
    @Test
    fun `Game viewModel - reset game with success result`() {
        viewModel.sessionId.set(correctSessionModel.deckId!!)
        viewModel.remainingCards.set(3)
        viewModel.allCardsInGame.set(200)

        mockSuccessShuffleCurrentDecks(correctSessionModel.deckId!!, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4C, cardJS, cardKD, cardAS)
        })

        playGame()

        assertEquals(viewModel.sessionId.get(), correctSessionModel.deckId)
        assertEquals(viewModel.allCardsInGame.get(), correctSessionModel.remaining)
        assertEquals(viewModel.remainingCards.get(), correctDealModel.remaining)
        assertEquals(viewModel.cards.size, correctDealModel.cards.size)
    }

    @Test
    fun `Game viewModel - reset game with error result 1`() {
        viewModel.sessionId.set(correctSessionModel.deckId!!)
        viewModel.remainingCards.set(3)
        viewModel.allCardsInGame.set(200)
        viewModel.cards.addAll(listOf(card4D, card4D))

        mockFailedShuffleCurrentDecks(correctSessionModel.deckId!!)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4C, cardJS, cardKD, cardAS)
        })

        playGame()

        assertEquals(viewModel.sessionId.get(), correctSessionModel.deckId)
        assertEquals(viewModel.allCardsInGame.get(), 200)
        assertEquals(viewModel.remainingCards.get(), 3)
        assertEquals(viewModel.cards.size, 2)
    }

    @Test
    fun `Game viewModel - reset game with error result 2`() {
        viewModel.sessionId.set(correctSessionModel.deckId!!)
        viewModel.remainingCards.set(3)
        viewModel.allCardsInGame.set(200)
        viewModel.cards.addAll(listOf(card4D, card4D))

        mockSuccessShuffleCurrentDecks(correctSessionModel.deckId!!, correctSessionModel)
        mockFailedDrawCards(correctSessionModel.deckId!!, 5)

        playGame()

        assertEquals(viewModel.sessionId.get(), correctSessionModel.deckId)
        assertEquals(viewModel.allCardsInGame.get(), correctSessionModel.remaining)
        assertEquals(viewModel.remainingCards.get(), correctSessionModel.remaining)
        assertEquals(viewModel.cards.size, 2)
    }

    //Result Games
    @Test
    fun `Game viewModel - No composition`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4C, cardJS, cardKD, cardAS)
        })

        playGame()

        assertFalse(viewModel.hasTwins.get()!!)
        assertFalse(viewModel.hasColor.get()!!)
        assertFalse(viewModel.hasFigures.get()!!)
    }

    @Test
    fun `Game viewModel - Twins result composition`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4C, card4D, cardKD, cardAS)
        })

        playGame()

        assertTrue(viewModel.hasTwins.get()!!)
        assertFalse(viewModel.hasColor.get()!!)
        assertFalse(viewModel.hasFigures.get()!!)
    }

    @Test
    fun `Game viewModel - Color result composition`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4D, cardAS, cardKD, cardKD)
        })

        playGame()

        assertFalse(viewModel.hasTwins.get()!!)
        assertTrue(viewModel.hasColor.get()!!)
        assertFalse(viewModel.hasFigures.get()!!)
    }

    @Test
    fun `Game viewModel - Figures result composition`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4D, cardQC, cardKD, cardJS)
        })

        playGame()

        assertFalse(viewModel.hasTwins.get()!!)
        assertFalse(viewModel.hasColor.get()!!)
        assertTrue(viewModel.hasFigures.get()!!)
    }

    @Test
    fun `Game viewModel - Twins and color results compositions`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4H, card4H, card4H, cardKD, cardJS)
        })

        playGame()

        assertTrue(viewModel.hasTwins.get()!!)
        assertTrue(viewModel.hasColor.get()!!)
        assertFalse(viewModel.hasFigures.get()!!)
    }

    @Test
    fun `Game viewModel - Twins and figures results compositions`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(cardQC, cardQC, cardQD, cardKD, cardJS)
        })

        playGame()

        assertTrue(viewModel.hasTwins.get()!!)
        assertFalse(viewModel.hasColor.get()!!)
        assertTrue(viewModel.hasFigures.get()!!)
    }

    @Test
    fun `Game viewModel - Color and figures results compositions`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(card4C, card4C, cardQC, cardKD, cardJS)
        })

        playGame()

        assertFalse(viewModel.hasTwins.get()!!)
        assertTrue(viewModel.hasColor.get()!!)
        assertTrue(viewModel.hasFigures.get()!!)
    }

    @Test
    fun `Game viewModel - Twins, figures and color results compositions`() {
        mockSuccessShuffleNewDecks(1, correctSessionModel)
        mockSuccessDrawCards(correctSessionModel.deckId!!, 5, correctDealModel.apply {
            cards = listOf(cardQC, cardQC, cardQC, cardKD, cardJS)
        })

        playGame()?.assertValue {
            when (it) {
                is Success -> it.data == correctDealModel
                is Failed -> false
            }
        }

        assertTrue(viewModel.hasTwins.get()!!)
        assertTrue(viewModel.hasColor.get()!!)
        assertTrue(viewModel.hasFigures.get()!!)
    }

    private fun mockSuccessShuffleNewDecks(deckCount : Int, success: GameSessionApiModel) {
        `when`(mainService.shuffleNewDecks(deckCount))
            .thenReturn(Single.just(Result.Success(success)))
    }

    private fun mockFailedShuffleNewDecks(deckCount : Int) {
        `when`(mainService.shuffleNewDecks(deckCount))
            .thenReturn(Single.just(Result.Failed(Throwable("failed"))))
    }

    private fun mockSuccessDrawCards(deckId: String, count: Int, success: GameDealApiModel) {
        `when`(mainService.drawCards(deckId,count))
            .thenReturn(Single.just(Result.Success(success)))
    }

    private fun mockFailedDrawCards(deckId: String, count: Int) {
        `when`(mainService.drawCards(deckId, count))
            .thenReturn(Single.just(Result.Failed(Throwable("failed"))))
    }

    private fun mockSuccessShuffleCurrentDecks(deckId: String, success: GameSessionApiModel) {
        `when`(mainService.shuffleCurrentDecks(deckId))
            .thenReturn(Single.just(Result.Success(success)))
    }

    private fun mockFailedShuffleCurrentDecks(deckId: String) {
        `when`(mainService.shuffleCurrentDecks(deckId))
            .thenReturn(Single.just(Result.Failed(Throwable("failed"))))
    }

    private fun playGame(): TestObserver<Result<GameDealApiModel>>? {
        val testObserver = viewModel.playGame().test()
        testSchedulerRule.testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        return testObserver.assertNoErrors()
    }
}