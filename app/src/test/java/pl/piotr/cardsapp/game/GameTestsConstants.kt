package pl.piotr.cardsapp.game

import pl.piotr.cardsapp.shared.services.api.models.game.CardApiModel
import pl.piotr.cardsapp.shared.services.api.models.game.GameDealApiModel
import pl.piotr.cardsapp.shared.services.api.models.game.GameSessionApiModel

class GameTestsConstants {

    companion object {

        // Session
        val correctSessionModel = GameSessionApiModel().apply {
            deckId = "exampleId"
            remaining = 52
            success = true
            shuffled = true
        }

        //Cards
        val card4H = CardApiModel().apply {
            suit = "HEARTS"
            code = "4H"
            value = "4"
            image = "https://deckofcardsapi.com/static/img/4H.png"
        }
        val card4C = CardApiModel().apply {
            suit = "CLUBS"
            code = "4C"
            value = "4"
            image = "https://deckofcardsapi.com/static/img/4C.png"
        }

        val card4D = CardApiModel().apply {
            suit = "DIAMONDS"
            code = "4D"
            value = "4"
            image = "https://deckofcardsapi.com/static/img/4D.png"
        }

        val cardAS = CardApiModel().apply {
            suit = "SPADES"
            code = "AS"
            value = "ACE"
            image = "https://deckofcardsapi.com/static/img/AS.png"
        }

        val cardQC = CardApiModel().apply {
            suit = "CLUBS"
            code = "QC"
            value = "QUEEN"
            image = "https://deckofcardsapi.com/static/img/QC.png"
        }

        val cardQD = CardApiModel().apply {
            suit = "DIAMONDS"
            code = "QD"
            value = "QUEEN"
            image = "https://deckofcardsapi.com/static/img/QD.png"
        }

        val cardKD = CardApiModel().apply {
            suit = "DIAMONDS"
            code = "KD"
            value = "KING"
            image = "https://deckofcardsapi.com/static/img/KD.png"
        }

        val cardJS = CardApiModel().apply {
            suit = "SPADES"
            code = "JS"
            value = "QUEEN"
            image = "https://deckofcardsapi.com/static/img/JS.png"
        }

        //Deal
        val correctDealModel = GameDealApiModel().apply {
            deckId = "exampleId"
            remaining = 4
            success = true
        }
    }
}