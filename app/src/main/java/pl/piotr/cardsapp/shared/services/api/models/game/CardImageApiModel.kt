package pl.piotr.cardsapp.shared.services.api.models.game

class CardImageApiModel {

    var png: String? = null

    var svg: String? = null
}