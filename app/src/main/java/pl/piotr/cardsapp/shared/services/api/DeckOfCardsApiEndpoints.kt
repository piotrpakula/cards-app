package pl.piotr.cardsapp.shared.services.api

import pl.piotr.cardsapp.shared.services.api.models.game.GameDealApiModel
import pl.piotr.cardsapp.shared.services.api.models.game.GameSessionApiModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DeckOfCardsApiEndpoints {

    @GET("deck/new/shuffle/")
    fun shuffleNewDecks(@Query("deck_count") deckCount: Int): Call<GameSessionApiModel>

    @GET("deck/{deck_id}/draw/")
    fun drawCards(
        @Path("deck_id") deckId: String,
        @Query("count") count: Int
    ): Call<GameDealApiModel>

    @GET("deck/{deck_id}/shuffle/")
    fun shuffleCurrentDecks(@Path("deck_id") deckId: String): Call<GameSessionApiModel>
}