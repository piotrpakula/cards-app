package pl.piotr.cardsapp.shared.extensions

import android.databinding.ObservableList

infix fun <T> ObservableList<T>.onPropertiesChanged(callback: (List<T>) -> Unit) {
    addOnListChangedCallback(object : ObservableList.OnListChangedCallback<ObservableList<T>>(){
        override fun onItemRangeRemoved(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
            sender?.let { callback(it) }
        }

        override fun onItemRangeMoved(sender: ObservableList<T>?, fromPosition: Int, toPosition: Int, itemCount: Int) {
            sender?.let { callback(it) }
        }

        override fun onItemRangeInserted(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
            sender?.let { callback(it) }
        }

        override fun onItemRangeChanged(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
            sender?.let { callback(it) }
        }

        override fun onChanged(sender: ObservableList<T>?) {
            sender?.let { callback(it) }
        }
    })
}