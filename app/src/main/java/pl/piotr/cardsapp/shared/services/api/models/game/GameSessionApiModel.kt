package pl.piotr.cardsapp.shared.services.api.models.game

import com.google.gson.annotations.SerializedName

class GameSessionApiModel {

    var shuffled: Boolean? = null

    var success: Boolean? = null

    var remaining: Int? = null

    @SerializedName("deck_id")
    var deckId: String? = null
}