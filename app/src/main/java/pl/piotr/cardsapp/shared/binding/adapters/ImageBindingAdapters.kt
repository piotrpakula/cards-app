package pl.piotr.cardsapp.shared.binding.adapters

import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.Glide
import android.databinding.BindingAdapter
import android.widget.ImageView

@BindingAdapter(value = ["android:src"])
fun loadImage(view: ImageView, image: String?) {
    image?.let {
        Glide
            .with(view.context)
            .load(image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .fitCenter()
            .into(view)
    }
}