package pl.piotr.cardsapp.shared.rx

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class DisposeBag {

    var compositeDisposable: CompositeDisposable? = null

    operator fun plusAssign(disposable: Disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable?.add(disposable)
    }

    fun dispose() {
        compositeDisposable?.dispose()
        compositeDisposable = null
    }
}