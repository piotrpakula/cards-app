package pl.piotr.cardsapp.shared.di.modules

import dagger.Module
import dagger.Provides
import pl.piotr.cardsapp.shared.services.api.GameApiService
import pl.piotr.cardsapp.shared.services.api.DeckOfCardsApiService

@Module
class AppServiceModule {

    @Provides
    fun provideApiService(): GameApiService {
        return DeckOfCardsApiService()
    }
}