package pl.piotr.cardsapp.shared.binding

fun safeUnbox(boxed: Int?): Int {
    return boxed ?: 0
}

fun safeUnbox(boxed: Long?): Long {
    return boxed ?: 0L
}

fun safeUnbox(boxed: Short?): Short {
    return boxed ?: 0
}

fun safeUnbox(boxed: Byte?): Byte {
    return boxed ?: 0
}

fun safeUnbox(boxed: Char?): Char {
    return boxed ?: '\u0000'
}

fun safeUnbox(boxed: Double?): Double {
    return boxed ?: 0.0
}

fun safeUnbox(boxed: Float?): Float {
    return boxed ?: 0f
}

fun safeUnbox(boxed: Boolean?): Boolean {
    return boxed ?: false
}