package pl.piotr.cardsapp.shared.services.api.models

sealed class Result<out S> {

    data class Success<out S>(val data: S) : Result<S>()
    data class Failed<T>(val error: Throwable) : Result<T>()

    val isSuccess get() = this is Success
    val isFailed get() = this is Failed

    fun either(onSuccess: (S) -> Any, onFailed: (Throwable) -> Any): Any =
        when (this) {
            is Success -> onSuccess(data)
            is Failed -> onFailed(error)
        }
}

typealias Success<S> = Result.Success<S>
typealias Failed<T> = Result.Failed<T>