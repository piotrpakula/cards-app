package pl.piotr.cardsapp.shared.extensions

import android.app.Activity
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog

fun Activity.simpleAlert(@StringRes message: Int) {
    AlertDialog.Builder(this).apply { setMessage(message) }.show()
}