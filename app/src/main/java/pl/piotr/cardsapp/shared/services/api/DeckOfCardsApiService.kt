package pl.piotr.cardsapp.shared.services.api

import com.google.gson.GsonBuilder
import io.reactivex.Single
import pl.piotr.cardsapp.BuildConfig
import pl.piotr.cardsapp.shared.services.api.models.Result
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class DeckOfCardsApiService @Inject constructor() : GameApiService {

    private val api: DeckOfCardsApiEndpoints

    init {
        val gson = GsonBuilder()
            .setLenient()
            .create()

        api = Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(DeckOfCardsApiEndpoints::class.java)
    }

    override fun shuffleNewDecks(deckCount: Int) = asSingleRequest(api.shuffleNewDecks(deckCount))

    override fun drawCards(deckId: String, count: Int) = asSingleRequest(api.drawCards(deckId, count))

    override fun shuffleCurrentDecks(deckId: String) = asSingleRequest(api.shuffleCurrentDecks(deckId))

    private fun <T> asSingleRequest(request: Call<T>): Single<Result<T>> {
        return Single.create<Result<T>> {
            request.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    response.body()?.let { body ->
                        it.onSuccess(Result.Success(body))
                    } ?: run {
                        it.onSuccess(Result.Failed(Throwable("no content")))
                    }
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    it.onSuccess(Result.Failed(t))
                }
            })
        }
    }
}