package pl.piotr.cardsapp.shared.binding.adapters

import android.databinding.BindingAdapter
import android.view.View

@BindingAdapter(value = ["android:visibility"])
fun loadVisibility(view: View, visibility: Boolean?) {
    visibility?.let { view.visibility = if (it) View.VISIBLE else View.GONE }
}