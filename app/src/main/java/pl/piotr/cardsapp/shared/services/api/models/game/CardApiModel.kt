package pl.piotr.cardsapp.shared.services.api.models.game

class CardApiModel {

    var suit: String? = null

    var code: String? = null

//    var images: List<CardImageApiModel> = listOf()

    var value: String? = null

    var image: String? = null
}