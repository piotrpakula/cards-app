package pl.piotr.cardsapp.shared.di

import dagger.Component
import dagger.Reusable
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import pl.piotr.cardsapp.GlobalApplication
import pl.piotr.cardsapp.game.di.GameInjectors
import pl.piotr.cardsapp.shared.di.modules.AppServiceModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppServiceModule::class, GameInjectors::class])
interface AppComponent : AndroidInjector<GlobalApplication> {

    @Reusable
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<GlobalApplication>()
}