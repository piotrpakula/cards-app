package pl.piotr.cardsapp.shared.services.api

import io.reactivex.Single
import pl.piotr.cardsapp.shared.services.api.models.game.GameDealApiModel
import pl.piotr.cardsapp.shared.services.api.models.game.GameSessionApiModel
import pl.piotr.cardsapp.shared.services.api.models.Result

interface GameApiService {

    fun shuffleNewDecks(deckCount: Int): Single<Result<GameSessionApiModel>>

    fun drawCards(deckId: String, count: Int): Single<Result<GameDealApiModel>>

    fun shuffleCurrentDecks(deckId: String): Single<Result<GameSessionApiModel>>
}