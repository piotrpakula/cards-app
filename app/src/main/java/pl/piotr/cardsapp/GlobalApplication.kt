package pl.piotr.cardsapp

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import pl.piotr.cardsapp.shared.di.DaggerAppComponent


class GlobalApplication : DaggerApplication() {

    private val appComponent: AndroidInjector<GlobalApplication> by lazy {
        DaggerAppComponent.builder().create(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = appComponent
}