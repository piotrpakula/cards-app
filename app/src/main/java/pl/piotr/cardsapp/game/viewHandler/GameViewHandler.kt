package pl.piotr.cardsapp.game.viewHandler

interface GameViewHandler {

    fun onClickPlayGame()
}