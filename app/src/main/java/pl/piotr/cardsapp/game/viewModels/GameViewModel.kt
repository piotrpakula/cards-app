package pl.piotr.cardsapp.game.viewModels

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import io.reactivex.Observable
import pl.piotr.cardsapp.game.services.GameService
import pl.piotr.cardsapp.shared.extensions.onPropertiesChanged
import pl.piotr.cardsapp.shared.rx.DisposeBag
import pl.piotr.cardsapp.shared.services.api.models.Failed
import pl.piotr.cardsapp.shared.services.api.models.Result
import pl.piotr.cardsapp.shared.services.api.models.Success
import pl.piotr.cardsapp.shared.services.api.models.game.CardApiModel
import pl.piotr.cardsapp.shared.services.api.models.game.GameDealApiModel
import pl.piotr.cardsapp.shared.services.api.models.game.GameSessionApiModel
import javax.inject.Inject

class GameViewModel @Inject constructor(private val mainService: GameService) {

    companion object {
        private const val GameCardsCount = 5
        private const val GameDecksCount = 1
        private const val MinRemainingCards = 4

        private const val KingValue = "KING"
        private const val QueenValue = "QUEEN"
        private const val JackValue = "JACK"
    }

    val sessionId = ObservableField<String>()
    val allCardsInGame = ObservableField(0)
    val remainingCards = ObservableField(0)
    val cards = ObservableArrayList<CardApiModel>()

    val hasCards = ObservableField(false)
    val isExecuting = ObservableField(false)
    val hasFigures = ObservableField(false)
    val hasColor = ObservableField(false)
    val hasTwins = ObservableField(false)

    val disposeBag = DisposeBag()

    init {
        remainingCards.addOnPropertyChangedCallback(object :
            android.databinding.Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: android.databinding.Observable?, propertyId: Int) {
                remainingCards.get()?.let { hasCards.set(it > MinRemainingCards) }
            }
        })

        cards.onPropertiesChanged { prepareCardsComposition() }
    }

    fun playGame(): Observable<Result<GameDealApiModel>> {
        return when {
            allCardsInGame.get() == 0 -> startNewGame().flatMap {
                when(it) {
                    is Success -> drawCards()
                    is Failed -> Observable.just(Failed(Throwable("start game failed")))
                }
            }
            hasCards.get()?.let { it } ?: false -> drawCards()
            else -> resetGame().flatMap {
                when(it) {
                    is Success -> drawCards()
                    is Failed -> Observable.just(Failed(Throwable("reset game failed")))
                }
            }
        }
    }

    private fun prepareCardsComposition() {

        hasFigures.set(cards
            .filter { it.value == KingValue || it.value == QueenValue || it.value == JackValue }
            .size > 2)

        hasTwins.set(cards
            .groupingBy { it.value }
            .eachCount()
            .filter { it.value > 2 }
            .isNotEmpty())

        hasColor.set(cards
            .groupingBy { it.suit }
            .eachCount()
            .filter { it.value > 2 }
            .isNotEmpty())
    }

    private fun startNewGame(): Observable<Result<GameSessionApiModel>> {
        isExecuting.set(true)
        return mainService.shuffleNewDecks(GameDecksCount).toObservable().share().also {
            disposeBag += it.subscribe { response ->
                when (response) {
                    is Success -> {
                        response.data.deckId?.let { sessionId.set(it) }
                        response.data.remaining?.let {
                            allCardsInGame.set(it)
                            remainingCards.set(it)
                        }
                    }
                }
            }
        }
    }

    private fun drawCards(): Observable<Result<GameDealApiModel>> {
        isExecuting.set(true)
        return mainService.drawCards(sessionId.get()!!, GameCardsCount).toObservable().share().also {
            disposeBag += it.subscribe { response ->
                isExecuting.set(false)
                when (response) {
                    is Success -> {
                        response.data.cards.let {
                            cards.clear()
                            cards.addAll(it)
                        }
                        response.data.remaining?.let { remainingCards.set(it) }
                    }
                }
            }
        }
    }

    private fun resetGame(): Observable<Result<GameSessionApiModel>> {
        isExecuting.set(true)
        return mainService.shuffleCurrentDecks(sessionId.get()!!).toObservable().share().also {
            disposeBag += it.subscribe { response ->
                when (response) {
                    is Success -> {
                        response.data.remaining?.let {
                            allCardsInGame.set(it)
                            remainingCards.set(it)
                        }
                    }
                }
            }
        }
    }
}