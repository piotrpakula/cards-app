package pl.piotr.cardsapp.game.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.piotr.cardsapp.game.di.modules.GameActivityModule
import pl.piotr.cardsapp.game.views.GameActivity
import pl.piotr.cardsapp.shared.di.scopes.ActivityScope

@Module
abstract class GameInjectors {

    @ActivityScope
    @ContributesAndroidInjector(modules = [GameActivityModule::class])
    abstract fun mainActivity(): GameActivity
}