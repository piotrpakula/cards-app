package pl.piotr.cardsapp.game.adapters

import android.databinding.ObservableList
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.piotr.cardsapp.databinding.GameItemCardBinding
import pl.piotr.cardsapp.shared.services.api.models.game.CardApiModel

class GameCardsAdapter(val cards: ObservableList<CardApiModel>) :
    RecyclerView.Adapter<GameCardsAdapter.GameCardViewHolder>() {

    init {
        setHasStableIds(true)

        cards.addOnListChangedCallback(object : ObservableList.OnListChangedCallback<ObservableList<CardApiModel>>() {
            override fun onChanged(sender: ObservableList<CardApiModel>?) {
                this@GameCardsAdapter.notifyDataSetChanged()
            }

            override fun onItemRangeRemoved(sender: ObservableList<CardApiModel>?, positionStart: Int, itemCount: Int) {
                this@GameCardsAdapter.notifyItemRangeRemoved(positionStart, itemCount)
            }

            override fun onItemRangeMoved(
                sender: ObservableList<CardApiModel>?,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                this@GameCardsAdapter.notifyItemMoved(fromPosition, toPosition)
            }

            override fun onItemRangeInserted(
                sender: ObservableList<CardApiModel>?,
                positionStart: Int,
                itemCount: Int
            ) {
                this@GameCardsAdapter.notifyItemRangeInserted(positionStart, itemCount)
            }

            override fun onItemRangeChanged(sender: ObservableList<CardApiModel>?, positionStart: Int, itemCount: Int) {
                this@GameCardsAdapter.notifyItemRangeChanged(positionStart, itemCount)
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameCardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = GameItemCardBinding.inflate(inflater, parent, false)
        return GameCardViewHolder(binding)
    }

    override fun getItemCount(): Int = cards.size

    override fun onBindViewHolder(viewHolder: GameCardViewHolder, position: Int) {
        viewHolder.setViewModel(cards[position])
    }

    inner class GameCardViewHolder(val binding: GameItemCardBinding) : RecyclerView.ViewHolder(binding.root) {

        fun setViewModel(model: CardApiModel) {
            binding.viewModel = model
        }
    }
}
