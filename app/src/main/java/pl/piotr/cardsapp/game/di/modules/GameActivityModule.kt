package pl.piotr.cardsapp.game.di.modules

import dagger.Module
import dagger.Provides
import pl.piotr.cardsapp.game.services.DefaultGameService
import pl.piotr.cardsapp.game.services.GameService
import pl.piotr.cardsapp.game.viewModels.GameViewModel
import pl.piotr.cardsapp.shared.di.scopes.ActivityScope
import pl.piotr.cardsapp.shared.services.api.GameApiService


@Module
class GameActivityModule {

    @Provides
    @ActivityScope
    fun provideMainService(gameApiService: GameApiService): GameService {
        return DefaultGameService(gameApiService)
    }

    @Provides
    @ActivityScope
    fun provideViewModel(mainService: GameService): GameViewModel {
        return GameViewModel(mainService)
    }
}