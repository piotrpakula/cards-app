package pl.piotr.cardsapp.game.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.piotr.cardsapp.R
import pl.piotr.cardsapp.databinding.GameActivityBinding
import pl.piotr.cardsapp.game.adapters.GameCardsAdapter
import pl.piotr.cardsapp.game.viewHandler.GameViewHandler
import pl.piotr.cardsapp.game.viewModels.GameViewModel
import pl.piotr.cardsapp.shared.extensions.simpleAlert
import pl.piotr.cardsapp.shared.services.api.models.Failed
import pl.piotr.cardsapp.shared.services.api.models.game.CardApiModel
import javax.inject.Inject

class GameActivity : AppCompatActivity(), GameViewHandler {

    companion object {
        private const val SessionIdStateKey = "sessionId.state.key"
        private const val CardsStateKey = "cards.state.key"
        private const val RemainingCardsStateKey = "remainingCards.state.key"
        private const val AllCardsStateKey = "allCards.state.key"
    }

    @Inject
    lateinit var viewModel: GameViewModel

    private lateinit var binding: GameActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.game_activity)
        binding.viewHandler = this
        binding.viewModel = viewModel

        binding.cards.apply {
            layoutManager = GridLayoutManager(context, 3)
            adapter = GameCardsAdapter(viewModel.cards)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disposeBag.dispose()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        val newBundle = (outState ?: Bundle()).apply {
            putString(SessionIdStateKey, viewModel.sessionId.get())
            putString(CardsStateKey, Gson().toJson(viewModel.cards))
            putInt(RemainingCardsStateKey, viewModel.remainingCards.get() ?: 0)
            putInt(AllCardsStateKey, viewModel.allCardsInGame.get() ?: 0)
        }
        super.onSaveInstanceState(newBundle)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            if (it.containsKey(SessionIdStateKey)) {
                viewModel.sessionId.set(it.getString(SessionIdStateKey))
            }
            if (it.containsKey(CardsStateKey)) {
                val type = object : TypeToken<List<CardApiModel>>() {}.type
                viewModel.cards.addAll(Gson().fromJson(it.getString(CardsStateKey), type))
            }
            if (it.containsKey(RemainingCardsStateKey)) {
                viewModel.remainingCards.set(it.getInt(RemainingCardsStateKey))
            }
            if (it.containsKey(AllCardsStateKey)) {
                viewModel.allCardsInGame.set(it.getInt(AllCardsStateKey))
            }
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onClickPlayGame() {
        viewModel.disposeBag += viewModel.playGame()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                when (it) {
                    is Failed -> simpleAlert(R.string.error)
                }
            }
    }
}