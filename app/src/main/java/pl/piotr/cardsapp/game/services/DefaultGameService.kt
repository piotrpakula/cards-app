package pl.piotr.cardsapp.game.services

import pl.piotr.cardsapp.shared.services.api.GameApiService
import javax.inject.Inject

class DefaultGameService @Inject constructor(private val cardsApiService: GameApiService) : GameService {

    override fun shuffleNewDecks(deckCount: Int) = cardsApiService.shuffleNewDecks(deckCount)

    override fun drawCards(deckId: String, count: Int) = cardsApiService.drawCards(deckId, count)

    override fun shuffleCurrentDecks(deckId: String) = cardsApiService.shuffleCurrentDecks(deckId)
}